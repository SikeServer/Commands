package com.sikeserver.commands.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.commands.CommandsManager;
import com.sikeserver.core.CoreManager;

public class SpawnCommandExecutor implements CommandExecutor {
	private CommandsManager plugin;
	private CoreManager core;
	
	public SpawnCommandExecutor() {
		plugin = CommandsManager.getPlugin();
		core = plugin.core;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(core.getMessage("Error.Common.Console"));
			return true;
		}
		
		Player p = (Player)sender;
		p.teleport(p.getWorld().getSpawnLocation());
		
		return true;
	}

}
