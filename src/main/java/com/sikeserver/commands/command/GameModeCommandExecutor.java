package com.sikeserver.commands.command;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.commands.CommandsManager;
import com.sikeserver.core.CoreManager;

public class GameModeCommandExecutor implements CommandExecutor {
	private CommandsManager plugin;
	private CoreManager core;
	
	public GameModeCommandExecutor() {
		plugin = CommandsManager.getPlugin();
		core = plugin.core;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(core.getMessage("Error.Common.Console"));
			return true;
		}
		
		Player p = (Player)sender;
		if (!p.hasPermission("SikeServer.Commands.GameMode")) {
			p.sendMessage(core.getMessage("Error.Common.Permission"));
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("gms"))
			p.setGameMode(GameMode.SURVIVAL);
		else if (cmd.getName().equalsIgnoreCase("gmc"))
			p.setGameMode(GameMode.CREATIVE);
		else if (cmd.getName().equalsIgnoreCase("gmsp"))
			p.setGameMode(GameMode.SPECTATOR);
		
		return true;
	}
}