package com.sikeserver.commands;

import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.commands.command.GameModeCommandExecutor;
import com.sikeserver.commands.command.SpawnCommandExecutor;
import com.sikeserver.core.CoreManager;

public class CommandsManager extends JavaPlugin {
	public CoreManager core;
	private static CommandsManager plugin;
	
	@Override
	public void onEnable() {
		core = CoreManager.getPlugin();
		plugin = this;
		
		GameModeCommandExecutor executor = new GameModeCommandExecutor();
		getCommand("gms").setExecutor(executor);
		getCommand("gmc").setExecutor(executor);
		getCommand("gmsp").setExecutor(executor);
		
		getCommand("spawn").setExecutor(new SpawnCommandExecutor());
	}
	
	/**
	 * このクラスのインスタンスを投げます。
	 * 
	 * @author Siketyan
	 * 
	 * @return ry
	 */
	public static CommandsManager getPlugin() {
		return plugin;
	}
}
